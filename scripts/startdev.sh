#!/bin/bash

export FLASK_APP=app.py
export FLASK_ENV=development
export FLASK_DEBUG=1
dirpath=$(dirname $(which $0))

cd "$dirpath"/..

flask run
