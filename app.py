from flask import request, Flask
import requests as req
import os 
from datetime import datetime


app = Flask(__name__)
DIR_STORAGE = os.getenv("DIR_STORAGE")


def check_file_data(data, filename):
    with open(DIR_STORAGE + filename, 'r') as file:
        text = file.read()
        if text != data:
            raise Exception("the file data are not the same as the original data")


def choose_unique_name():
    fn = str(datetime.utcnow().timestamp()) + ".md"
    if os.path.isfile(DIR_STORAGE+fn):
       return choose_unique_name()
    return fn


def write_to_file(data, filename):
    with open(DIR_STORAGE + filename, 'w') as file:
        file.write(data)


def add_workdir():
    """
    this function add a working directory 
    if it doesn't exist
    """
    if not os.path.isdir(DIR_STORAGE):
        os.mkdir(DIR_STORAGE)

    
def run(request):
    data = request.form["data"]
    fn = choose_unique_name()
    add_workdir()
    write_to_file(data, fn)
    check_file_data(data, fn)


@app.route('/', methods=['POST'])
def main():
    try:
        run(request)
    except Exception as err:
        return "Error: " + str(err), 400

    return ""